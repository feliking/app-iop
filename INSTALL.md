# Instalación

## Requisitos
  - Nodejs 7.6 en adelante

## Modo de uso

Para instalar este paquete tiene que tener su llave pública de su equipo `cat ~/.ssh/id_rsa.pub` registrado en https://gitlab.geo.gob.bo

``` bash
# Instalando librería
npm install git+ssh://git@gitlab.agetic.gob.bo:base/app-iop.git --save
```

**Nota.-** Si la instalación del paquete no avanza, agregar manualmente la lláve privada de tu equipo a la conexión ssh:

``` bash
# Agregando llave privada
ssh-add ~/.ssh/id_rsa
```

Instanciando el módulo iop en un proyecto
  ``` js
const Iop = require('app-iop');
const config = {
  database: 'postgres',
  username: 'postgres',
  password: 'postgres',
  host: 'localhost'
};

// Para usar await debe estar dentro una función async
const iop = await Iop(config).catch(err => console.error(err));

// Lista completa de iop, puede recibir parámetros de búsqueda entre otras opciones
const list = await iop.findAll();
```

## Instalando Node.js v8.x para el modo desarrollo

NOTA.- Debian Wheezy no soporta Node 8

  ``` bash
# Para Ubuntu
curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
sudo apt-get install -y nodejs

# Para Debian, instalar como root
curl -sL https://deb.nodesource.com/setup_8.x | bash -
apt-get install -y nodejs
```

## Instalando el proyecto

Siga los siguientes pasos:

``` bash
# 1. Instalar dependencias
npm install

# 2. Correr test de prueba, configurar la conexión de la base de datos en el archivo src/util.js y agregar datos de test validos en tests/services-tests.js
npm test
```
