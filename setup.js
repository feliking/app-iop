'use strict';

const { config, handleFatalError } = require('./src/util');
const Iop = require('./');

async function setup () {
  const iop = await Iop(config).catch(handleFatalError);

  console.log('Services!', iop);
  console.log('Success Iop setup!');
  process.exit(0);
}
setup();
