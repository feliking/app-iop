'use strict';

const debug = require('debug')('app:iop:segip:persona');
const request = require('request');
module.exports = function segipServicePersona (iop) {
  function buscarPersonaSegip (ci, complemento, fechaNacimiento, url, token, onlyCI) {
    debug('SEGIP - Datos a buscar', ci, complemento, fechaNacimiento);
    return new Promise((resolve, reject) => {
      let urlServicio;
      if (onlyCI) {
        urlServicio = `${url}${ci}`;
      } else {
        if (complemento) {
          urlServicio = `${url}${ci}?fecha_nacimiento=${fechaNacimiento}&complemento=${complemento}`;
        } else {
          urlServicio = `${url}${ci}?fecha_nacimiento=${fechaNacimiento}`;
        }
      }
      debug('SEGIP - Petición:', urlServicio);
      let options = {
        url: urlServicio,
        method: 'GET',
        rejectUnauthorized: false,
        headers: {
          Authorization: `Bearer ${token}`,
          'Content-Type': 'application/json'
        },
        json: true
      };
      request.get(options, (error, response, body) => {
        debug('SEGIP - buscar persona Respuesta', body);
        if (error) {
          return reject(error);
        }
        if (typeof body === 'string' && body.match(/Not Found/) !== null && body.match(/Not Found/).length === 1) {
          return reject(new Error(`No se encontraron datos en SEGIP para el CI: ${ci}`));
        }
        if (body.res && body.res === 'err') {
          return reject(new Error(`No se encontraron datos en SEGIP para la fecha de nacimiento: ${fechaNacimiento}`));
        }
        if (typeof body.ConsultaDatoPersonaEnJsonResult === 'undefined') {
          return reject(new Error('Error con el servicio web SEGIP'));
        }
        if (body.ConsultaDatoPersonaEnJsonResult.DatosPersonaEnFormatoJson === 'null') {
          resolve({
            status: response.statusCode,
            persona: {
              estado: body.ConsultaDatoPersonaEnJsonResult.CodigoRespuesta,
              mensaje_estado: body.ConsultaDatoPersonaEnJsonResult.DescripcionRespuesta
            },
            mensaje: `Error con el estado: ${response.statusCode}`
          });
        }
        if (typeof body.ConsultaDatoPersonaEnJsonResult === 'undefined') {
          return reject(new Error('Error con el servicio web SEGIP'));
        }
        if (typeof body.ConsultaDatoPersonaEnJsonResult.DatosPersonaEnFormatoJson === 'undefined') {
          return reject(new Error('Error con el servicio web SEGIP'));
        }
        let persona = JSON.parse(body.ConsultaDatoPersonaEnJsonResult.DatosPersonaEnFormatoJson);
        if (!persona) {
          return reject(new Error(`No se encontró datos de esta persona en SEGIP con ese ci: ${ci} y/o fecha de nacimiento: ${fechaNacimiento}`));
        }

        let fechaMMddyy = persona.FechaNacimiento.split('/');
        let fechaYYmmdd = [fechaMMddyy[2], fechaMMddyy[1], fechaMMddyy[0]].join('-');
        if (response.statusCode === 200) {
          resolve({
            persona: {
              nombres: persona.Nombres,
              paterno: persona.PrimerApellido === '--' ? '' : persona.PrimerApellido,
              materno: persona.SegundoApellido === '--' ? '' : persona.SegundoApellido,
              tipo_documento: 'CI',
              nro_documento: persona.NumeroDocumento,
              complemento: persona.Complemento,
              lugar_expedicion: persona.LugarNacimientoDepartamento,
              nacionalidad: persona.LugarNacimientoPais,
              genero: '',
              fecha_nacimiento: fechaYYmmdd,
              estado: body.ConsultaDatoPersonaEnJsonResult.CodigoRespuesta,
              mensaje_estado: body.ConsultaDatoPersonaEnJsonResult.DescripcionRespuesta
            },
            mensaje: body.ConsultaDatoPersonaEnJsonResult.DescripcionRespuesta
          });
        } else {
          resolve({
            status: response.statusCode,
            mensaje: `Error con status: ${response.statusCode}`,
            data: body
          });
        }
      });
    });
  }

  async function buscarPersona (ci, fechaNacimiento, complemento, onlyCI = false) {
    debug('Busqueda de persona');
    const segip = await iop.findByCode('SEGIP-01');
    let item;
    try {
      item = await buscarPersonaSegip(ci, complemento, fechaNacimiento, segip.url, segip.token, onlyCI);
    } catch (e) {
      return e;
    }
    if (!item) {
      return { error: `Error al realizar la búsqueda en el SEGIP` };
    }
    if (item.count === 0) {
      return { error: `No existen los datos solicitados en el servicio de búsqueda del SEGIP` };
    }
    if (item.persona.estado !== '2') {
      if (item.persona.estado === '3' || item.persona.estado === '4') {
        let mensaje = 'Debe acudir al SEGIP para regularizar su situación.';
        if (item.persona.mensaje_estado.indexOf('FALLECIDO') !== -1) {
          mensaje = 'Lo sentimos mucho.';
        }
        return { error: `${item.persona.mensaje_estado} - ${mensaje}` };
      }
      return { error: `No existe resultado para su búsqueda en el SEGIP. Revise que los datos: ci, fecha de nacimiento sean correctos` };
    }

    return item;
  }
  return buscarPersona;
};
