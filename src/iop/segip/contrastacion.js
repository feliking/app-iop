'use strict';

const debug = require('debug')('app:iop:segip:contrastacion');
const request = require('request');

module.exports = function segipService (iop) {
  function contrastarInfoSegip (_listaCampo, tipoPersona, segipUrl, segipToken) {
    return new Promise((resolve, reject) => {
      const listaCampos = {
        NumeroDocumento: _listaCampo.numero_documento,
        Complemento: _listaCampo.complemento,
        Nombres: _listaCampo.nombres,
        PrimerApellido: _listaCampo.primer_apellido,
        SegundoApellido: _listaCampo.segundo_apellido,
        FechaNacimiento: _listaCampo.fecha_nacimiento,
        LugarNacimientoPais: _listaCampo.lugar_nacimiento_pais,
        LugarNacimientoDepartamento: _listaCampo.lugar_nacimiento_departamento,
        LugarNacimientoProvincia: _listaCampo.lugar_nacimiento_provincia,
        LugarNacimientoLocalidad: _listaCampo.lugar_nacimiento_localidad
      };
      const lista = JSON.stringify(listaCampos);
      const config = {
        url: `${segipUrl}?tipo_persona=${tipoPersona}&lista_campo=${lista}`,
        method: 'GET',
        rejectUnauthorized: false,
        headers: {
          Authorization: `Bearer ${segipToken}`,
          'Content-Type': 'application/json'
        },
        json: true
      };
      debug('Petición', config);
      request(config, (err, res, body) => {
        debug('Respuesta', body);
        if (err) {
          reject(err);
          return;
        }
        if (body && body.ConsultaDatoPersonaContrastacionResult) {
          body = body.ConsultaDatoPersonaContrastacionResult;

          let result = {
            status: body.CodigoRespuesta,
            data: null
          };
          if (body.ContrastacionEnFormatoJson) {
            result.data = JSON.parse(body.ContrastacionEnFormatoJson);
          } else {
            if (body.DescripcionRespuesta) {
              result.data = body.DescripcionRespuesta;
            }
          }
          resolve(result);
        } else {
          if (res.statusCode === 503) {
            reject(new Error('El servicio del SEGIP no está disponible en estos momentos.'));
          } else {
            reject(new Error('La respuesta no es correcta en el servicio de contrastación: '));
          }
        }
      });
    });
  }

  async function contrastacion (listaCampos, tipoPersona) {
    debug('Contrastación de persona');
    const segip = await iop.findByCode('SEGIP-02');
    let item;
    try {
      item = await contrastarInfoSegip(listaCampos, tipoPersona, segip.url, segip.token);
    } catch (e) {
      return e;
    }
    if (!item) {
      return { error: `Error al realizar la contrastación con segip` };
    }
    if (item.count === 0) {
      return { error: `No existen datos del servicio de contrastación` };
    }
    console.log('Estado devuelto en contrastacion', item);
    if (item.status === '2') {
      if (item.data) {
        return item.data;
      }
      return { error: 'No se puedo obtener los datos de la persona.' };
    } else if (item.status === '1') {
      return { error: 'La persona no existe en el SEGIP' };
    } else if (item.status === '3') {
      return { warning: 'Se encontró más de una persona con los mismos datos, necesita ingresar el complemento de su cédula de identidad' };
    } else if (item.status === '4') {
      if (item.data) {
        return { warning: item.data };
      }
      return { error: 'No se pudo identificar la observación en el SEGIP' };
    }
    return { error: 'No se puede realizar la consulta con el SEGIP, inténtelo más adelante.' };
  }

  return contrastacion;
};
