'use strict';

const debug = require('debug')('app:iop:sin:login');
const request = require('request');
module.exports = function sinService (iop) {
  async function loginSIN (nit, usuario, contrasena, url, token) {
    debug('SIN - Datos a buscar', nit, usuario, contrasena);
    const sin = await iop.findByCode('SIN-01');
    return new Promise((resolve, reject) => {
      debug('SIN - Petición:', url);
      const urlServicio = `${sin.url}login`;
      let options = {
        url: urlServicio,
        body: {
          nit, usuario, clave: contrasena
        },
        method: 'POST',
        rejectUnauthorized: false,
        headers: {
          Authorization: `Bearer ${sin.token}`,
          'Content-Type': 'application/json'
        },
        json: true
      };
      request.post(options, (error, response, body) => {
        debug('SIN - Autenticación con credenciales', body);
        if (error) {
          return reject(error);
        }
        if (!body.estado && body.message && body.message.match(/You cannot consume this service/) !== null && body.message.match(/You cannot consume this service/).length === 1) {
          return reject(new Error(`Error con el Servicio Web SIN: no tiene permisos para usar este servicio.`));
        }
        if (!body.estado && body.message && body.message.match(/no API found with those valuese/) !== null && body.message.match(/no API found with those valuese/).length === 1) {
          return reject(new Error(`Error con el Servicio Web SIN: no se encontró el servicio solicitado.`));
        }
        if (body.Autenticado) {
          resolve({
            status: response.statusCode,
            mensaje: response.body.mensaje,
            data: body
          });
        } else {
          return reject(new Error(body.Mensaje || 'Error con el servicio web SIN'));
        }
      });
    });
  }

  return loginSIN;
};
