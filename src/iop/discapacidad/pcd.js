'use strict';

const debug = require('debug')('app:iop:discapacidad:pcd');
const request = require('request');

module.exports = function pcdService (iop) {
  async function validarPCD (nroDocumento) {
    debug('PCD - Datos a buscar', nroDocumento);
    const pcd = await iop.findByCode('PCD-01');
    return new Promise((resolve, reject) => {
      if (!pcd) {
        reject(new Error('PCD-000: El servicio PCD no se encuentra registrado en la base de datos. Consulte con el administrador del sistema.'));
      }
      debug('PCD - Petición:', `${pcd.url}${nroDocumento}`);
      const urlServicio = `${pcd.url}${nroDocumento}`;
      let options = {
        url: urlServicio,
        method: 'GET',
        rejectUnauthorized: false,
        headers: {
          Authorization: `Bearer ${pcd.token}`,
          'Content-Type': 'application/json'
        },
        json: true
      };
      request.get(options, (error, response, body) => {
        debug('PCD - Verificación de persona con discapacidad', body);
        if (error) {
          return reject(error);
        }
        if (!body) {
          return reject(new Error(`PCD-001: Error con el Servicio de Personas con Discapacidad: No se encontró ningún resultado.`));
        }
        if (body.message && body.message.match(/No credentials found for given 'iss'/) !== null && body.message.match(/No credentials found for given 'iss'/).length === 1) {
          return reject(new Error(`PCD-002: Error con el Servicio de Personas con Discapacidad: no tiene permisos para usar este servicio.`));
        }
        if (body.message && body.message.match(/Unauthorized/) !== null && body.message.match(/Unauthorized/).length === 1) {
          return reject(new Error(`PCD-003: Error con el Servicio de Personas con Discapacidad: no tiene permisos para usar este servicio.`));
        }
        if (body.message && body.message.match(/You cannot consume this service/) !== null && body.message.match(/You cannot consume this service/).length === 1) {
          return reject(new Error(`PCD-004: Error con el Servicio de Personas con Discapacidad: no tiene permisos para usar este servicio.`));
        }
        if (body.message && body.message.match(/Bad token; invalid JSON/) !== null && body.message.match(/Bad token; invalid JSON/).length === 1) {
          return reject(new Error(`PCD-005: Error con el Servicio de Personas con Discapacidad: no tiene permisos para usar este servicio.`));
        }
        if (body.message && body.message.match(/no API found with those valuese/) !== null && body.message.match(/no API found with those valuese/).length === 1) {
          return reject(new Error(`PCD-006: Error con el Servicio de Personas con Discapacidad: no se encontró el servicio solicitado.`));
        }
        if (body.mensaje && body.mensaje.match(/La ruta o el verbo utilizado no existen./) !== null && body.mensaje.match(/La ruta o el verbo utilizado no existen./).length === 1) {
          return reject(new Error(`PCD-007: Error con el Servicio de Personas con Discapacidad: no se encontró el servicio solicitado.`));
        }
        if (body.codigo === 1) {
          resolve(body);
        } else {
          return reject(new Error(body.mensaje || body.error || 'PCD-008 - Error con el Servicio de Personas con Discapacidad'));
        }
      });
    });
  }
  return validarPCD;
};
