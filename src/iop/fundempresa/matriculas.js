'use strict';

const debug = require('debug')('app:iop:fundempresa:matricula');
const request = require('request');

module.exports = function fundempresaService (iop) {
  function getMatriculas (nit, fndUrl, fndToken) {
    return new Promise((resolve, reject) => {
      var req = {
        url: `${fndUrl}nits/${nit}/matriculas/`,
        method: 'GET',
        rejectUnauthorized: false,
        headers: {
          Authorization: `Bearer ${fndToken}`,
          'Content-Type': 'application/json'
        },
        json: true
      };
      request.get(req, function (error, response, body) {
        if (error) {
          reject(error);
          return;
        }
        var lasMatriculas = [];
        if (body && body.detalle && body.detalle.infoNit && body.detalle.infoNit.length && body.error === '0000') {
          body.detalle.infoNit.forEach(function (matricula) {
            lasMatriculas.push({
              matricula: (parseInt(matricula.IdMatricula)).toString(),
              razon_social: matricula.RazonSocial
            });
          });
        } else {
          reject(new Error('No se encontraron matrículas asociadas al NIT.'));
          return;
        }
        if (response.statusCode === 503) {
          reject(new Error('El servicio de FUNDEMPRESA no está disponible en estos momentos.'));
          return;
        }

        resolve(lasMatriculas);
      });
    });
  }

  async function buscarMatriculas (nit) {
    debug('Buscando matrículas por nit');
    const fundempresa = await iop.findByCode('FUNDEMPRESA-02');
    let lista;
    try {
      lista = await getMatriculas(nit, fundempresa.url, fundempresa.token);
    } catch (e) {
      return e;
    }

    if (!lista) {
      return { error: `Error al obtener la lista de matrículas` };
    }

    if (lista.length === 0) {
      return { error: `No existen matrículas asociado al nit: ${nit}` };
    }

    return lista;
  }

  return buscarMatriculas;
};
