'use strict';

const debug = require('debug')('app:iop:fundempresa:matricula');
const request = require('request');

module.exports = function fundempresaService (iop) {
  function getInfoMatricula (matricula, fndUrl, fndToken) {
    return new Promise((resolve, reject) => {
      var req = {
        url: `${fndUrl}matriculas/${matricula}`,
        method: 'GET',
        rejectUnauthorized: false,
        headers: {
          Authorization: `Bearer ${fndToken}`,
          'Content-Type': 'application/json'
        },
        json: true
      };
      request.get(req, function (error, response, body) {
        if (error) {
          reject(error);
          return;
        }
        var infoMatricula = {};
        if (body && body.detalle && body.detalle.infoMatricula && body.error === '0000') {
          infoMatricula = Object.assign({}, body.detalle.infoMatricula); // No se reescriben el formato de las propiedades para no generar confusión con la documentación del API
          infoMatricula.tipo_societario = obtenerTipoSocietario(body.detalle.infoMatricula.TipoSocietario); // Pero sí se agrega el valor del tipo societario en texto
        } else {
          reject(new Error('No se encontró información para la matrícula.'));
          return;
        }
        if (response.statusCode === 503) {
          reject(new Error('El servicio de FUNDEMPRESA no está disponible en estos momentos.'));
          return;
        }

        resolve(infoMatricula);
      });
    });
  }

  async function obtenerMatricula (matricula) {
    debug('Obteniendo datos de una matrícula');
    const fundempresa = await iop.findByCode('FUNDEMPRESA-02');
    let data;
    try {
      data = await getInfoMatricula(matricula, fundempresa.url, fundempresa.token);
    } catch (e) {
      return e;
    }

    if (!data) {
      return { error: `Error al obtener la información de la matrícula` };
    }

    return data;
  }

  function obtenerTipoSocietario (TipoSocietario) {
    /* Tipo Societario de la matrícula, según IOP:
    01 EMPRESA UNIPERSONAL,
    03 SOCIEDAD COLECTIVA,
    04 SOCIEDAD DE RESPONSABILIDAD LIMITADA,
    05 SOCIEDAD EN COMANDITA SIMPLE,
    06 SOCIEDAD EN COMANDITA POR ACCIONES,
    07 SOCIEDAD ANONIMA, 08 SOCIEDAD ANONIMA MIXTA,
    09 SOCIEDAD CONSTITUIDA EN EL EXTRANJERO,
    10 ENTIDAD FINANCIERA DE VIVIENDA */
    let returnTipoSoc = null;
    switch (TipoSocietario) {
      case '01':
        returnTipoSoc = 'EMPRESA UNIPERSONAL';
        break;
      case '03':
        returnTipoSoc = 'SOCIEDAD COLECTIVA';
        break;
      case '04':
        returnTipoSoc = 'SOCIEDAD DE RESPONSABILIDAD LIMITADA';
        break;
      case '05':
        returnTipoSoc = 'SOCIEDAD EN COMANDITA SIMPLE';
        break;
      case '06':
        returnTipoSoc = 'SOCIEDAD EN COMANDITA POR ACCIONES';
        break;
      case '07':
        returnTipoSoc = 'SOCIEDAD ANONIMA';
        break;
      case '08':
        returnTipoSoc = 'SOCIEDAD ANONIMA MIXTA';
        break;
      case '09':
        returnTipoSoc = 'SOCIEDAD CONSTITUIDA EN EL EXTRANJERO';
        break;
      case '10':
        returnTipoSoc = 'ENTIDAD FINANCIERA DE VIVIENDA';
        break;
      default:
        returnTipoSoc = 'NO RECONOCIDO';
    }
    return returnTipoSoc;
  }

  return obtenerMatricula;
};
