'use strict';

module.exports = function setupIOP (iop) {
  return {
    segip: {
      buscarPersona: require('./segip/persona')(iop),
      contrastacion: require('./segip/contrastacion')(iop)
    },
    fundempresa: {
      matriculas: require('./fundempresa/matriculas')(iop),
      matricula: require('./fundempresa/infoMatricula')(iop)
    },
    sin: {
      login: require('./sin/login')(iop)
    },
    discapacidad: {
      validarPCD: require('./discapacidad/pcd')(iop)
    }
  };
};
