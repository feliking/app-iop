'use strict';

const { GraphQLScalarType } = require('graphql');
const { Kind } = require('graphql/language');
const { permissions } = require('./util');

module.exports = ServicioIop => {
  const schemes = [`
    # Escalar tipo Fecha
    scalar DateI

    # ServiciosIop del sistema
    type ServicioIop {
      # id del ServicioIop
      id: ID!
      # Código del ServicioIop
      codigo: String!
      # Nombre del método del ServicioIop
      metodo: String!
      # descripcion del ServicioIop
      descripcion: String
      # entidad del ServicioIop
      entidad: String
      # url del ServicioIop
      url: String!
      # token del ServicioIop
      token: String!
      # tipo de servicioIop
      tipo: TipoServicioIop
      # estado de servicioIop
      estado: EstadoServicioIop
      # Usuario que creo el registro
      _user_created: Int
      # Usuario que actualizó el registro
      _user_updated: Int
      # Fecha de creación del registro
      _created_at: DateI
      # Fecha de actualización del registro
      _updated_at: DateI
    }

    # Tipos de estado del serviciosIop
    enum TipoServicioIop {
      # Tipo PUBLICO
      PUBLICO
      # Tipo CONVENIO
      CONVENIO
    } 

    # Tipos de estado del serviciosIop
    enum EstadoServicioIop {
      # Estado ACTIVO
      ACTIVO
      # Estado INACTIVO
      INACTIVO
    } 

    # Objeto para crear un ServicioIop
    input NewServicioIop {
      codigo: String!
      metodo: String!
      descripcion: String
      entidad: String
      url: String!
      token: String!
      tipo: TipoServicioIop
    }

    # Objeto para editar un ServicioIop
    input EditServicioIop {
      codigo: String
      metodo: String
      descripcion: String
      entidad: String
      url: String
      token: String
      tipo: TipoServicioIop
      estado: EstadoServicioIop
    }

    # Objeto de paginación para ServicioIop
    type ServiciosIop {
      count: Int 
      rows: [ServicioIop]
    }

    # Objeto de respuesta de objeto eliminado
    type DeleteServicioIop {
      deleted: Boolean
    }
  `];

  const queries = {
    Query: `
      # Lista de serviciosIop
      serviciosIop(
        # Límite de la consulta para la paginación
        limit: Int, 
        # Nro. de página para la paginación
        page: Int, 
        # Campo a ordenar, "-campo" ordena DESC
        order: String, 
        # Buscar por codigo de ServicioIop
        codigo: String
        # Buscar por metodo de ServicioIop
        metodo: String
        # Buscar por el descripcion del ServicioIop
        descripcion: String
        # Buscar por el entidad del ServicioIop
        entidad: String
        # Buscar por el url del ServicioIop
        url: String
        # Buscar por el tipo del ServicioIop
        tipo: TipoServicioIop
        # Buscar por el estado del ServicioIop
        estado: EstadoServicioIop
      ): ServiciosIop
      # Obtener un servicioIop
      servicioIop(id: Int!): ServicioIop
    `,
    Mutation: `
      # Agregar servicioIop
      servicioIopAdd(servicioIop: NewServicioIop!): ServicioIop
      # Editar servicioIop
      servicioIopEdit(id: Int!, servicioIop: EditServicioIop!): ServicioIop
      # Eliminar servicioIop
      servicioIopDelete(id: Int!): DeleteServicioIop
    `
  };

  // Cargando Resolvers
  const resolvers = {
    Query: {
      serviciosIop: (_, args, context) => {
        permissions(context, 'serviciosIop:read');

        return ServicioIop.findAll(args, context.id_rol);
      },
      servicioIop: (_, args, context) => {
        permissions(context, 'serviciosIop:read');

        return ServicioIop.findById(args.id);
      }
    },
    Mutation: {
      servicioIopAdd: (_, args, context) => {
        permissions(context, 'serviciosIop:create');

        args.servicioIop._user_created = context.id_usuario;
        return ServicioIop.createOrUpdate(args.servicioIop);
      },
      servicioIopEdit: (_, args, context) => {
        permissions(context, 'serviciosIop:update');

        args.servicioIop._user_updated = context.id_usuario;
        args.servicioIop._updated_at = new Date();
        args.servicioIop.id = args.id;
        return ServicioIop.createOrUpdate(args.servicioIop);
      },
      servicioIopDelete: async (_, args, context) => {
        permissions(context, 'serviciosIop:delete');

        let deleted = await ServicioIop.deleteItem(args.id);
        return { deleted };
      }
    },
    DateI: new GraphQLScalarType({
      name: 'DateI',
      description: 'DateI custom scalar type',
      parseValue (value) {
        return new Date(value); // value from the client
      },
      serialize (value) {
        // return moment(value).format('DD/MM/YYYY, h:mm a'); // value sent to the client
        return value;
      },
      parseLiteral (ast) {
        if (ast.kind === Kind.INT) {
          return parseInt(ast.value, 10); // ast value is always in string format
        }
        return null;
      }
    })
  };

  return {
    schemes,
    queries,
    resolvers
  };
};
