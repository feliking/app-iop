'use strict';

const { setTimestamps } = require('./util');

module.exports = (sequelize, DataTypes) => {
  let fields = {
    id: {
      primaryKey: true,
      autoIncrement: true,
      type: DataTypes.INTEGER
    },
    codigo: {
      type: DataTypes.STRING(20),
      unique: true,
      allowNull: false
    },
    metodo: {
      type: DataTypes.STRING(100),
      allowNull: false
    },
    descripcion: {
      type: DataTypes.TEXT
    },
    entidad: {
      type: DataTypes.STRING(255)
    },
    url: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    token: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    tipo: {
      type: DataTypes.ENUM,
      values: ['PUBLICO', 'CONVENIO'],
      defaultValue: 'CONVENIO',
      allowNull: false
    },
    estado: {
      type: DataTypes.ENUM,
      values: ['ACTIVO', 'INACTIVO'],
      defaultValue: 'ACTIVO',
      allowNull: false
    }
  };

  // Agregando campos para el log
  fields = setTimestamps(fields);

  let serviciosIop = sequelize.define('servicios_iop', fields, {
    timestamps: false,
    tableName: 'servicios_iop'
  });

  return serviciosIop;
};
