'use strict';

const { getQuery, errorHandler, deleteItemModel } = require('./util');

module.exports = function paramsServices (iop, Sequelize) {
  const Op = Sequelize.Op;

  function findAll (params = {}) {
    let query = getQuery(params);
    query.where = {};

    if (params.codigo) {
      query.where.codigo = {
        [Op.iLike]: `%${params.codigo}%`
      };
    }

    if (params.metodo) {
      query.where.metodo = {
        [Op.iLike]: `%${params.metodo}%`
      };
    }

    if (params.descripcion) {
      query.where.descripcion = {
        [Op.iLike]: `%${params.descripcion}%`
      };
    }

    if (params.entidad) {
      query.where.entidad = {
        [Op.iLike]: `%${params.entidad}%`
      };
    }

    if (params.url) {
      query.where.url = {
        [Op.iLike]: `%${params.url}%`
      };
    }

    if (params.tipo) {
      query.where.tipo = params.tipo;
    }

    if (params.estado) {
      query.where.estado = params.estado;
    }

    return iop.findAndCountAll(query);
  }

  function findById (id) {
    return iop.findOne({
      where: {
        id
      }
    });
  }

  function findByCode (codigo) {
    return iop.findOne({
      where: {
        codigo
      }
    });
  }

  async function createOrUpdate (data) {
    const cond = {
      where: {
        id: data.id
      }
    };

    const item = await iop.findOne(cond);

    if (item) {
      const updated = await iop.update(data, cond);
      return updated ? iop.findOne(cond) : item;
    }

    let result;
    try {
      result = await iop.create(data);
    } catch (e) {
      errorHandler(e);
    }

    return result.toJSON();
  }

  async function deleteItem (id) {
    return deleteItemModel(id, iop);
  }

  return {
    findAll,
    findById,
    deleteItem,
    createOrUpdate,
    findByCode
  };
};
