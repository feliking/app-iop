# Módulo servicios iop

Creación, búsqueda y listado de servicios iop (Interoperabilidad) registrandolo en una tabla con Sequelize, Postgresql y Graphql.

En `src/model` se define la tabla `servicios_iop` con varios campos entre los que están:

- `metodo`: Especifica el método HTTP a usar
- `url`: La URI del servicio
- `token`: Token de acceso al servicio
- `codigo`: Identificador del servicio aquí se usa codificación a conveniencia estos códigos deben ser tal cúal están en los archivos dentro de `src/iop`. Por ejemplo para el servicio discapacidad existe un servicio con el código 'PCD-01' y se busca por este código ver en el archivo [pcd.js](src/iop/discapacidad/pcd.js). Cada servicio debe tener su código único asignado manualmente.
- ...

En el proyecto que se incluya, se debe llenar estos valores en la tabla `servicios_iop` con los servicios de interoperabilidad necesarios.

* Instalación: [INSTALL.md](INSTALL.md)
* Licencia: [LPG Bolivia](https://web.archive.org/web/20180903234734/https://softwarelibre.gob.bo/licencia.php)

## Lista de contribución

- Omar Gutierrez <ogutierrez@agetic.gob.bo>
- Ronald Vallejos <rvallejos@agetic.gob.bo>
- Alvaro Mamani <almamani@agetic.gob.bo>
- Diego F. Ticona Ramos <dticona@agetic.gob.bo>
- Judith Calizaya <jcalizaya@agetic.gob.bo>
- Rodrigo Garcia <rgarcia@agetic.gob.bo>
