'use strict';

const test = require('ava');
const { config, handleFatalError } = require('../src/util');
const Iop = require('../');

let serviciosIop;

test.beforeEach(async () => {
  if (!serviciosIop) {
    // config.force = true; // Forzando la eliminación de la tabla
    serviciosIop = await Iop(config).catch(handleFatalError);
  }
});

test.serial('ServicioIop#findAll', async t => {
  let lista = await serviciosIop.findAll();

  t.true(lista.count >= 1, 'Se tiene más de un registro');
});

test.serial('ServicioIop#segip - persona', async t => {
  const nroDocumento = '00000000';
  const fechaNacimiento = '01/01/1999';
  let persona = await serviciosIop.segip.buscarPersona(nroDocumento, fechaNacimiento);

  t.is(nroDocumento, persona.persona.nro_documento, 'Respuesta segip - número de documento.');
  t.is(fechaNacimiento.split('/').reverse().join('-'), persona.persona.fecha_nacimiento, 'Respuesta segip - fecha de nacimiento.');
});

test.serial('ServicioIop#segip - persona error', async t => {
  const nroDocumento = '000000';
  const fechaNacimiento = '01/01/1999';
  let persona = await serviciosIop.segip.buscarPersona(nroDocumento, fechaNacimiento);

  t.true(!!persona.error, 'Respuesta segip - número de documento.');
});

test.serial('ServicioIop#segip - persona contrastación', async t => {
  const data = {};
  data.numero_documento = '0000000';
  data.complemento = '1B';
  data.fecha_nacimiento = '01/01/1999';
  data.primer_apellido = 'APELLIDO';
  data.segundo_apellido = 'SEG APELLIDO';
  data.nombres = 'NOMBRES';
  let persona = await serviciosIop.segip.contrastacion(data, 1);
  t.true(Object.keys(persona).length >= 6, 'Se encontró un registro persona ú documento');
});

test.serial('ServicioIop#segip - persona contrastación not exist', async t => {
  const data = {};
  data.numero_documento = '00000000';
  data.complemento = '1B';
  data.fecha_nacimiento = '01/01/19999';
  data.primer_apellido = 'MAMANI';
  data.segundo_apellido = 'LAURA';
  data.nombres = 'BLANCA';
  let persona = await serviciosIop.segip.contrastacion(data, 1);
  t.is('La persona no existe en el SEGIP', persona.error, 'No existe la persona');
});

test.serial('ServicioIop#segip - persona contrastación observado', async t => {
  const data = {};
  data.numero_documento = '0000000';
  data.complemento = '1G';
  data.fecha_nacimiento = '01/01/19999';
  data.primer_apellido = 'APELLIDO';
  data.segundo_apellido = 'SEG APELLIDO';
  data.nombres = 'NOMBRES';
  let persona = await serviciosIop.segip.contrastacion(data, 1);
  t.true(persona.warning.indexOf('observación:') !== -1, 'Persona con observación');
});

test.serial('ServicioIop#segip - persona contrastación duplicidad', async t => {
  const data = {};
  data.numero_documento = '000000';
  // data.complemento = '1I';
  data.fecha_nacimiento = '01/01/1999';
  data.primer_apellido = 'APELLIDO';
  data.segundo_apellido = 'SEG APELLIDO';
  data.nombres = 'NOMBRES';
  let persona = await serviciosIop.segip.contrastacion(data, 1);
  t.is('Se encontró más de una persona con los mismos datos, necesita ingresar el complemento de su cédula de identidad', persona.warning, 'Persona duplicada');
});

// Servicio de SIN con Error
test.serial('ServicioIop#sin - login', async t => {
  const nit = '00000';
  const usuario = '00000';
  const clave = '00000';
  try {
    await serviciosIop.sin.login(nit, usuario, clave);
  } catch (e) {
    t.is(e.message, 'Alguno de los datos es incorrecto', 'Respuesta SIN - error');
  }
});

// Servicio de SIN - autenticación con éxito
test.serial('ServicioIop#sin - login', async t => {
  const nit = '0000000';
  const usuario = 'usuario';
  const clave = 'clave-usuario';
  const loginResult = await serviciosIop.sin.login(nit, usuario, clave);
  t.is(loginResult.data.Estado, 'ACTIVO HABILITADO', 'Respuesta SIN - login');
});

// Servicio de Validación de discapacidad - Éxito
test.serial('\nServicioIop#discapacidad - pdc - éxito', async t => {
  const nroDocumento = '0000000';
  let respuesta = await serviciosIop.discapacidad.validarPCD(nroDocumento);
  t.is(nroDocumento, respuesta.datos.cedulaIdentidad, 'Respuesta PCD - número de documento.');
});

// Servicio de Validación de discapacidad - No se encuentra el registro
test.serial('\nServicioIop#discapacidad - pdc - no existe', async t => {
  const nroDocumento = '111222333';
  try {
    await serviciosIop.discapacidad.validarPCD(nroDocumento);
  } catch (e) {
    t.is(e.message, 'No se encontró información con el ci indicado.', 'La persona no es encuentra en el servicio PCD.');
  }
});

// Servicio de Validación de discapacidad - No se encuentra el registro
test.serial('\nServicioIop#discapacidad - pdc - no existe', async t => {
  const nroDocumento = '111222333*';
  try {
    await serviciosIop.discapacidad.validarPCD(nroDocumento);
  } catch (e) {
    t.regex(e.message, /de Identidad inválido$/, 'Error en el formato del Carnet de Identidad.');
  }
});
